## Desafio nodejs

## Testando as rotas

Foi disponibilizado una collection do Postman para os testes

1. Importar arquivo de collection do Postman [desafio_node.postman_collection.json]
2. Após importação da collection, verá que todas rotas possuem nome explicativo de suas chamadas
3. Execute as rotas [1 - User signup | 2 - User login] passando um email e senha
4. Copie o token que é retornado no [2 - User login] e cole-o na aba **Authorization** do Postman para executar com sucesso as rotas que requerem autenticação do usuário
	- Rotas com autenticação:
		- Save product
		- Edit one product
		- Delete one product
		- Delete all

---

## Executando testes unitários

1. Faça download do repositorio do projeto
2. Execute o comando 'npm install' para instalar todas dependências do projeto
3. Foi utilizado o framework de testes unitários Mocha
4. Dentro da pasta do projeto, execute o comando de teste: 'mocha --timeout 15000'
5. Será executado 5 testes unitários que devem passar

---