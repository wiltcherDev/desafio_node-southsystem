let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

let token = '';
let _id = '';
let url = 'https://desafio-node-south-system.herokuapp.com';
before((done) => {
  let user = {
    email: "test@test.com",
    password: "123456789"
  }
  chai.request(url)
    .post('/api/auth/login')
    .send(user)
    .end((err, res) => {
      res.should.have.status(200);
      token = res.body.token;
      done();
    });
});

beforeEach((done) => {
  chai.request(url)
    .delete('/api/catalog/')
    .set('Authorization', 'Bearer ' + token)
    .end((err, res) => {
      res.should.have.status(200);
      done();
    });
});

describe('/POST api/catalog/', () => {
  it('Should add product and return status 201', (done) => {
    let _product = {
      codReference: "djsaoi45",
      product: "Kindle Paperwhite",
      price: 230.56,
      availability: "Em estoque"
    }
    chai.request(url)
      .post('/api/catalog/')
      .set('Authorization', 'Bearer ' + token)
      .send(_product)
      .end((err, res) => {
        res.should.have.status(201);
        done();
      });
  });
});

describe('/GET api/catalog/', () => {
  it('Should return 0 products', (done) => {
    chai.request(url)
      .get('/api/catalog/page/1')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.lengthOf(0);
        done();
      });
  });

  it('Should return 1 product', (done) => {
    let _product = {
      codReference: "djsaoi45",
      product: "Kindle Paperwhite",
      price: 230.56,
      availability: "Em estoque"
    }
    chai.request(url)
      .post('/api/catalog/')
      .set('Authorization', 'Bearer ' + token)
      .send(_product)
      .end((err, res) => {
        res.should.have.status(201);
        
        chai.request(url)
          .get('/api/catalog/page/1')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.lengthOf(1);
            done();
          });
      });
    
  });
});

describe('/DELETE api/catalog/', () => {
  it('Should delete product', (done) => {
    let _product = {
      codReference: "djsaoi45",
      product: "Kindle Paperwhite",
      price: 230.56,
      availability: "Em estoque"
    }
    chai.request(url)
      .post('/api/catalog/')
      .set('Authorization', 'Bearer ' + token)
      .send(_product)
      .end((err, res) => {
        res.should.have.status(201);

        chai.request(url)
          .get('/api/catalog/page/1')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.lengthOf(1);
            _id = res.body[0]._id;
            
            chai.request(url)
              .delete('/api/catalog/'+_id)
              .set('Authorization', 'Bearer ' + token)
              .end((err, res) => {
                res.should.have.status(200);
                
                chai.request(url)
                  .get('/api/catalog/page/1')
                  .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.lengthOf(0);
                    done();
                  });
              });
          });
      });
  });
});

describe('/PUT api/catalog/', () => {
  it('Should update product', (done) => {
    let _product = {
      codReference: "djsaoi45",
      product: "Kindle Paperwhite",
      price: 230.56,
      availability: "Em estoque"
    }
    let _newProduct = {
      codReference: "asdfgyu54",
      product: "Kindle Usado",
      price: 30.56,
      availability: "Em estoque"
    }
    chai.request(url)
      .post('/api/catalog/')
      .set('Authorization', 'Bearer ' + token)
      .send(_product)
      .end((err, res) => {
        res.should.have.status(201);

        chai.request(url)
          .get('/api/catalog/page/1')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.lengthOf(1);
            _id = res.body[0]._id;

            chai.request(url)
              .put('/api/catalog/'+_id)
              .set('Authorization', 'Bearer ' + token)
              .send(_newProduct)
              .end((err, res) => {
                res.should.have.status(201);
                
                chai.request(url)
                  .get('/api/catalog/'+_id)
                  .end((err, res) => {
                    res.should.have.status(200);
                    res.body.product.should.be.equal(_newProduct.product);
                    done();
                  });
              });
          });
      });
  });
});