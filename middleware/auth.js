const jwt = require('jsonwebtoken');

/**
 * Captura a autorização do cabeçalho e verifica se é uma autorização válida para as rotas protegidas
 */
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, '1f15294ef394e3d2874f1bf4159d6750');
        const userId = decodedToken.userId;
        if (req.body.userId && req.body.userId !== userId) {
            throw 'Invalid user ID';
        } else {
            next();
        }
    } catch {
        res.status(401).json({
            error: new Error('Invalid request!')
        });
    }
};