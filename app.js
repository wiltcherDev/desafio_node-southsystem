const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const catalogRoutes = require('./routes/catalog');
const userRoutes = require('./routes/user');

const app = express();

/**
 * Conexão com o banco 
 */
mongoose.connect('mongodb+srv://Sandy:0iibtAa1U6KPwidZ@cluster0-tntnf.mongodb.net/test?retryWrites=true&w=majority')
    .then(() => {
        console.log('Connected to MongoDB');
    })
    .catch((error) => {
        console.log('Unable to connect to MongoDB');
        console.error(error);
    });

/**
 * Cabeçalho CORS que permite que todas as solicitações de todas as origens tenham acesso
 */
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});
app.use(bodyParser.json());

app.use('/api/catalog', catalogRoutes);
app.use('/api/auth', userRoutes);

module.exports = app;