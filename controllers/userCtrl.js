const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const validEmail = /\S+@\S+/;

/**
 * Salva um usuario para aplicação
 */
exports.signup = (req, res, next) => {
    if(!req.body.password || !req.body.email){
        return res.status(400).json({
            error: 'Email and password are requered!'
        });
    }
    if(!validEmail.test(String(req.body.email).toLowerCase())){
        return res.status(400).json({
            error: 'Email is not invalid'
        });
    }
    bcrypt.hash(req.body.password, 10).then(
        (hash) => {
            const user = new User({
                email: req.body.email,
                password: hash
            });
            user.save().then(
                () => {
                    res.status(201).json({
                        message: 'User added successfully!'
                    });
                }
            ).catch(
                (error) => {
                    res.status(500).json({
                        error: error
                    });
                }
            );
        }
    );
};

/**
 * Efetua o login na aplicação, enviando um token para autenticação nas rotas
 */
exports.login = (req, res, next) => {
    if(!req.body.password || !req.body.email){
        return res.status(400).json({
            error: 'Email and password are requered!'
        });
    }
    if(!validEmail.test(String(req.body.email).toLowerCase())){
        return res.status(400).json({
            error: 'Email is not invalid'
        });
    }
    User.findOne({ email: req.body.email }).then(
        (user) => {
            if (!user) {
                return res.status(401).json({
                    error: 'User not found!'
                });
            }
            bcrypt.compare(req.body.password, user.password).then(
                (valid) => {
                    if (!valid) {
                        return res.status(401).json({
                            error: 'Incorrect password!'
                        });
                    }
                    const token = jwt.sign(
                        { userId: user._id },
                        '1f15294ef394e3d2874f1bf4159d6750',
                        { expiresIn: '24h' });
                    res.status(200).json({
                        userId: user._id,
                        token: token
                    });
                }
            ).catch(
                (error) => {
                    res.status(500).json({
                        error: error
                    });
                }
            );
        }
    ).catch(
        (error) => {
            res.status(500).json({
                error: error
            });
        }
    );
}