const Product = require('../models/product');

/**
 * Cria um modelo de produto recebido e o salva no banco de dados
 */
exports.createProduct = (req, res, next) => {
    const product = new Product({
        codReference: req.body.codReference,
        product: req.body.product,
        price: req.body.price,
        availability: req.body.availability
    });
    product.save().then(
        () => {
            res.status(201).json({
                message: 'Product saved successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

/**
 * Faz a busca por um produto passando seu _id
 */
exports.getOneProduct = (req, res, next) => {
    Product.findOne({
        _id: req.params.id
    }).then(
        (product) => {
            res.status(200).json(product);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

/**
 * Pesquisa um produto passando um de seus atributos na pesquisa
 */
exports.searchProduct = (req, res, next) => {
    var query = {};
    if (req.body.product) {
        query.product = {$regex: req.body.product};
    }
    if (req.body.codReference) {
        query.codReference = {$regex: req.body.codReference};
    }
    if (req.body.price) {
        query.price = {$regex: req.body.price};
    }
    if (req.body.availability) {
        query.availability = {$regex: req.body.availability};
    }
    Product.find(query).then(
        (product) => {
            res.status(200).json(product);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};
/**
 * Cria um modelo de produto recebido e o atualiza no banco de dados
 */
exports.editProduct = (req, res, next) => {
    const product = new Product({
        _id: req.params.id,
        codReference: req.body.codReference,
        product: req.body.product,
        price: req.body.price,
        availability: req.body.availability
    });
    Product.updateOne({ _id: req.params.id }, product).then(
        () => {
            res.status(201).json({
                message: 'Product updated successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

/**
 * Deleta um produto no banco passando seu _id
 */
exports.deleteProduct = (req, res, next) => {
    Product.deleteOne({ _id: req.params.id }).then(
        () => {
            res.status(200).json({
                message: 'Product deleted successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

/**
 * Deleta todos os produtos do banco
 */
exports.deleteAllProduct = (req, res, next) => {
    Product.deleteMany({}).then(
        () => {
            res.status(200).json({
                message: 'Products deleted successfully!'
            });
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};

/**
 * Retorna todos os produtos do banco
 */
exports.getAllProducts = (req, res, next) => {
    let page = req.params.page;
    let limit = 5;
    let skip = limit * (page - 1);
    Product.find().skip(skip).limit(limit).then(
        (product) => {
            res.status(200).json(product);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};