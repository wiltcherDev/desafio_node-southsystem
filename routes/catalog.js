const express = require('express');
const router = express.Router();

const auth = require('../middleware/auth');
const catalogCtrl = require('../controllers/catalogCtrl');

router.get('/page/:page', catalogCtrl.getAllProducts);
router.post('/', auth, catalogCtrl.createProduct);
router.get('/:id', catalogCtrl.getOneProduct);
router.put('/:id', auth, catalogCtrl.editProduct);
router.delete('/:id', auth, catalogCtrl.deleteProduct);
router.delete('/', auth, catalogCtrl.deleteAllProduct);
router.post('/search', catalogCtrl.searchProduct);

module.exports = router;