const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    codReference: { type: String, required: true },
    product: { type: String, required: true },
    price: { type: Number, required: true },
    availability: { type: String, required: true }
});

module.exports = mongoose.model('Product', productSchema);